//
//  Car.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import ObjectMapper

class Car: Mappable {
    var model: String!
    var price: Float!
    var pictureURL: NSURL!
    var passengers: Int!
    var luggage: Int!
    var carClass: String!
    var id: Int!
    
    var json: AnyObject!
    
    required init() {}
    
    //MARK: - Mappable
    func mapping(map: Map) {
        model       <- map["car_model"]
        price       <- (map["car_total_price"], FloatFromStringTransform())
        pictureURL  <- (map["car_picture"], URLTransform())
        passengers  <- (map["car_people"], IntFromStringTransform())
        luggage     <- (map["car_luggage"], IntFromStringTransform())
        carClass    <- map["car_class"]
        id          <- map["car_id"]
    }
}

class IntFromStringTransform: TransformType {
    typealias Object = Int
    typealias JSON = String
    
    init() {}
    
    func transformFromJSON(value: AnyObject?) -> Object? {
        if let value = value as? String {
            return value.toInt()
        }
        return nil
    }
    
    func transformToJSON(value: Object?) -> String? {
        if let value = value {
            return value.description
        }
        return nil
    }
}