//
//  ProfileViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ProfileViewController: UIViewController {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "ProfileViewController", bundle: NSBundle.mainBundle())
    }
    
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var profileView: ProfileView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadAppearance()
    }

    func reloadAppearance() {
        labelPhone.text = Defaults[kDGTPhoneNumberKey].string
        profileView.reloadAppearance()
    }
}
