//
//  AppDelegate.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import TwitterKit
import SwiftyUserDefaults

let BASE_URL = "http://104.245.38.26/~rustemr/"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Twitter.sharedInstance().startWithConsumerKey("sJmdLeDuOggcQmCVdpYmKuaUF",
            consumerSecret: "D0iGh103zYLuFVD4u3D1PmVrPwRAkO4NGLj0CVl3WZ0YtBfxsX")
        Fabric.with([Crashlytics(), Twitter.sharedInstance()])
        
        var navigationController = self.window!.rootViewController! as UINavigationController
        navigationController.setNavigationBarHidden(true, animated: false)
        
        //changing to logged in flow
        if Defaults.hasKey(kDGTUserIDKey) {
            navigationController.viewControllers = [RRTabController.tripDerController()]
            TwitterHelper.sharedInstance.refreshUserInfo(nil)
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

