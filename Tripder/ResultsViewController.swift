//
//  ResultsViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "ResultsViewController", bundle: NSBundle.mainBundle())
    }
    @IBOutlet weak var constraintButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonNext: UIButton!

    @IBOutlet weak var tableViewPackages: UITableView!
    var labelTotalPrice: UILabel!
    
    var hotels: [Hotel] = []
    var cars: [Car] = []
    
    var flightPack: FlightPack?
    var currentHotel: Hotel!
    var currentCar: Car!
    var user: TwitterUser!
    
    var showsOnlyCurrent = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewPackages.rowHeight = 88
        
        if showsOnlyCurrent {
            buttonNext.hidden = true
            constraintButtonHeight.constant = 0
            tableViewPackages.scrollEnabled = false
            configureUserHeaderView()
        } else {
            configureHeaderView()
        }
        configureFooterView()
        
        if currentHotel == nil {
            currentHotel = hotels.first
        }
        if currentCar == nil {
            currentCar = cars.first
        }
        reloadTotal()
    }
    
    func configureUserHeaderView() {
        var view = UIView(frame: CGRectMake(0, 0, self.view.bounds.width, 50))
        
        var imageBackground = UIImageView()
        imageBackground.contentMode = .ScaleAspectFill
        imageBackground.clipsToBounds = true
        imageBackground.sd_setImageWithURL(self.user.backgroundImageURL)
        view.addSubview(imageBackground)
        imageBackground.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[b]-(0)-|", options: .AlignAllBaseline,
            metrics: nil, views: ["b": imageBackground]))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[b]-(0)-|", options: .AlignAllBaseline,
            metrics: nil, views: ["b": imageBackground]))
        var visualEffect = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
        visualEffect.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(visualEffect)
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[b]-(0)-|", options: .AlignAllBaseline,
            metrics: nil, views: ["b": visualEffect]))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[b]-(0)-|", options: .AlignAllBaseline,
            metrics: nil, views: ["b": visualEffect]))
        
        var labelName = UILabel()
        labelName.text = self.user.name
        labelName.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 20)
        labelName.backgroundColor = UIColor.clearColor()
        labelName.setTranslatesAutoresizingMaskIntoConstraints(false)
        labelName.textColor = UIColor.whiteColor()
        view.addSubview(labelName)
        labelName.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraint(NSLayoutConstraint(item: labelName, attribute: .CenterY, relatedBy: .Equal,
            toItem: view, attribute: .CenterY, multiplier: 1, constant: 0))
        
        var imageUserpic = UIImageView()
        imageUserpic.layer.cornerRadius = 20
        imageUserpic.layer.masksToBounds = true
        imageUserpic.sd_setImageWithURL(self.user.profileImageURL)
        view.addSubview(imageUserpic)
        imageUserpic.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraint(NSLayoutConstraint(item: imageUserpic, attribute: .CenterY, relatedBy: .Equal,
            toItem: view, attribute: .CenterY, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(5)-[i(40)]-(8)-[l]",
            options: .AlignAllBaseline, metrics: nil, views: ["i": imageUserpic, "l": labelName]))
        imageUserpic.addConstraint(NSLayoutConstraint(item: imageUserpic, attribute: .Height, relatedBy: .Equal,
            toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 40))
        
        tableViewPackages.tableHeaderView = view
    }
    
    func configureHeaderView() {
        var view = UIView(frame: CGRectMake(0, 0, self.view.bounds.width, 60))
        
        var labelConfigure = UILabel()
        labelConfigure.text = "Configure package"
        labelConfigure.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 20)
        labelConfigure.backgroundColor = UIColor.clearColor()
        labelConfigure.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(labelConfigure)
        view.addConstraint(NSLayoutConstraint(item: labelConfigure, attribute: .CenterX, relatedBy: .Equal, toItem: view, attribute: .CenterX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: labelConfigure, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: 0))
        
        tableViewPackages.tableHeaderView = view
    }

    func configureFooterView() {
        var view = UIView(frame: CGRectMake(0, 0, self.view.bounds.width, 50))
        view.backgroundColor = UIColor(white: 0.89, alpha: 1)
        labelTotalPrice = UILabel()
        labelTotalPrice.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 22)
        labelTotalPrice.backgroundColor = UIColor.clearColor()
        labelTotalPrice.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(labelTotalPrice)
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[l]-(12)-|", options: .AlignAllBaseline, metrics: nil, views: ["l": labelTotalPrice]))
        view.addConstraint(NSLayoutConstraint(item: labelTotalPrice, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: 0))
        
        var labelTotal = UILabel()
        labelTotal.text = "TOTAL"
        labelTotal.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 22)
        labelTotal.backgroundColor = UIColor.clearColor()
        labelTotal.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(labelTotal)
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(12)-[l]", options: .AlignAllBaseline, metrics: nil, views: ["l": labelTotal]))
        view.addConstraint(NSLayoutConstraint(item: labelTotal, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: 0))
        
        tableViewPackages.tableFooterView = view
    }
    
    func reloadTotal() {
        var total = (currentHotel?.price ?? 0) + (currentCar?.price ?? 0) + (flightPack?.price ?? 0)
        labelTotalPrice.text = String(format: "$%.0f", total)
    }
    
    @IBAction func clickNext(sender: AnyObject) {
        var paymentController = PaymentViewController()
        paymentController.flightPack = self.flightPack
        paymentController.hotel = currentHotel
        paymentController.car = currentCar
        self.navigationController!.pushViewController(paymentController, animated: true)
    }
}

extension ResultsViewController: OptionsTableViewControllerDelegate {
    func optionsController(controller: OptionsTableViewController, didSelectOption option: AnyObject) {
        switch controller.optionsType! {
        case .Hotels:
            currentHotel = option as Hotel
        case .Cars:
            currentCar = option as Car
        }
        
        tableViewPackages.reloadData()
        reloadTotal()
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension ResultsViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (hotels.count > 0 ? 1 : 0) + (cars.count > 0 ? 1 : 0) + (flightPack != nil ? 1 : 0)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view = UIView(frame: CGRectMake(0, 0, 320, 30))
        view.backgroundColor = UIColor(white: 0.89, alpha: 1)
        var label = UILabel(frame: CGRectMake(12, 0, 200, 30))
        label.backgroundColor = view.backgroundColor
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)
        switch section {
        case 0:
            label.text = hotels.count > 0 ? "Hotel" : (cars.count > 0 ? "Car" : "Flights")
        case 1:
            label.text = (cars.count > 0 ? "Car" : "Flights")
        default:
            label.text = "Flights"
        }
        view.addSubview(label)
        return view
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if hotels.count > 0 {
                var cell = NSBundle.mainBundle().loadNibNamed("HotelTableViewCell", owner: self, options: nil)[0] as HotelTableViewCell
                cell.hotel = currentHotel
                if showsOnlyCurrent {
                    cell.selectionStyle = .None
                }
                return cell
            } else if cars.count > 0 {
                var cell = NSBundle.mainBundle().loadNibNamed("CarTableViewCell", owner: self, options: nil)[0] as CarTableViewCell
                cell.car = currentCar
                if showsOnlyCurrent {
                    cell.selectionStyle = .None
                }
                return cell
            } else {
                var cell = NSBundle.mainBundle().loadNibNamed("FlightsTableViewCell", owner: self, options: nil)[0] as FlightsTableViewCell
                cell.flightPack = flightPack
                if showsOnlyCurrent {
                    cell.selectionStyle = .None
                }
                return cell
            }
        case 1:
            if cars.count > 0 {
                var cell = NSBundle.mainBundle().loadNibNamed("CarTableViewCell", owner: self, options: nil)[0] as CarTableViewCell
                cell.car = currentCar
                if showsOnlyCurrent {
                    cell.selectionStyle = .None
                }
                return cell
            } else {
                var cell = NSBundle.mainBundle().loadNibNamed("FlightsTableViewCell", owner: self, options: nil)[0] as FlightsTableViewCell
                cell.flightPack = flightPack
                if showsOnlyCurrent {
                    cell.selectionStyle = .None
                }
                return cell
            }
        default:
            var cell = NSBundle.mainBundle().loadNibNamed("FlightsTableViewCell", owner: self, options: nil)[0] as FlightsTableViewCell
            cell.flightPack = flightPack
            if showsOnlyCurrent {
                cell.selectionStyle = .None
            }
            return cell
        }
    }
}

extension ResultsViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if showsOnlyCurrent {
            return
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var optionsController = OptionsTableViewController()
        switch indexPath.section {
        case 0:
            if hotels.count == 0 && cars.count == 0 {
                return
            }
            optionsController.optionsType = hotels.count > 0 ? .Hotels : .Cars
            optionsController.options = hotels.count > 0 ? hotels : cars
        case 1:
            if cars.count == 0 {
                return
            }
            optionsController.optionsType = .Cars
            optionsController.options = cars
        default:
            return
        }
        optionsController.delegate = self
        var navigationController = UINavigationController(rootViewController: optionsController)
        self.navigationController?.presentViewController(navigationController, animated: true, completion: nil)
    }
}
