//
//  Flight.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import ObjectMapper

class FlightPack: Mappable {
    var flight1: Flight!
    var flight2: Flight!
    var id: Int!
    var price: Float!
    var pictureURL: NSURL!
    
    var json: AnyObject!
    
    required init() {}
    
    //MARK: - Mappable
    func mapping(map: Map) {
        flight1     <- map["flight_1"]
        flight2     <- map["flight_2"]
        id          <- map["flight_pack_id"]
        price       <- (map["flight_price_value"], FloatFromStringTransform())
        pictureURL  <- (map["picture"], URLTransform())
    }
}

class Flight: Mappable {
    var departureCode: String!
    var arrivalCode: String!
    var departureDatetime: NSDate!
    var carrier: String!
    var number: String!
    
    required init() {}
    
    //MARK: - Mappable
    func mapping(map: Map) {
        departureCode       <- map["departure_airport_code"]
        arrivalCode         <- map["arrival_airport_code"]
        departureDatetime   <- (map["departure_time"], DateFromISOStringTransform())
        carrier             <- map["carrier_code"]
        number              <- map["flight_number"]
    }
}

let isoDateFormatter: NSDateFormatter = {
    var formatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    return formatter
}()

class DateFromISOStringTransform: TransformType {
    typealias Object = NSDate
    typealias JSON = String
    
    init() {}
    
    func transformFromJSON(value: AnyObject?) -> Object? {
        if let value = value as? JSON {
            return isoDateFormatter.dateFromString(value)
        }
        return nil
    }
    
    func transformToJSON(value: Object?) -> JSON? {
        if let value = value {
            return isoDateFormatter.stringFromDate(value)
        }
        return nil
    }
}