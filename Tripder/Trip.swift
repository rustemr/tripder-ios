//
//  Trip.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import ObjectMapper

class Trip: Mappable {
    
    var flightPack: FlightPack!
    var car: Car!
    var hotel: Hotel!
    var screenName: String! {
        didSet {
            TwitterHelper.sharedInstance.loadUser(screenName, completion: { user in
                self.user = user
            })
        }
    }
    var user: TwitterUser!
    
    required init() { }
    
    //MARK: - Mappable
    func mapping(map: Map) {
        flightPack  <- map["flights"]
        car         <- map["cars"]
        hotel       <- map["hotels"]
        screenName  <- map["user.user_name"]
    }
}
