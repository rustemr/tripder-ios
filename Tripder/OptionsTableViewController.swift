//
//  OptionsTableViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

enum OptionsType: String {
    case Hotels = "Hotels", Cars = "Cars"
}

protocol OptionsTableViewControllerDelegate: NSObjectProtocol {
    func optionsController(controller: OptionsTableViewController, didSelectOption option: AnyObject)
}

class OptionsTableViewController: UITableViewController {
    
    var options = [AnyObject]()
    var optionsType: OptionsType!
    
    weak var delegate: OptionsTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 88
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .Plain, target: self, action: "clickClose:")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = self.optionsType.rawValue
    }
    
    func clickClose(sender: UIBarButtonItem!) {
        self.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch optionsType! {
        case .Hotels:
            var cell = NSBundle.mainBundle().loadNibNamed("HotelTableViewCell", owner: self, options: nil)[0] as HotelTableViewCell
            cell.hotel = options[indexPath.row] as Hotel
            return cell
        case .Cars:
            var cell = NSBundle.mainBundle().loadNibNamed("CarTableViewCell", owner: self, options: nil)[0] as CarTableViewCell
            cell.car = options[indexPath.row] as Car
            return cell
        }
    }
    
    // MARK: Table view delegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.delegate?.optionsController(self, didSelectOption: options[indexPath.row])
    }
}
