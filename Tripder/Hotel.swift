//
//  Hotel.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import ObjectMapper

class Hotel: Mappable {
    var title: String!
    var price: Float!
    var pictureURL: NSURL!
    var rating: Float!
    var description: String!
    var id: Int!
    
    var json: AnyObject!
    
    required init() {}
    
    //MARK: - Mappable
    func mapping(map: Map) {
        title       <- map["hotel_title"]
        price       <- (map["hotel_total_price"], FloatFromStringTransform())
        pictureURL  <- (map["hotel_picture"], URLTransform())
        rating      <- (map["hotel_rating"], FloatFromStringTransform())
        description <- map["hotel_descr"]
        id          <- map["hotel_id"]
    }
}

class FloatFromStringTransform: TransformType {
	typealias Object = Float
	typealias JSON = String

	init() {}

	func transformFromJSON(value: AnyObject?) -> Object? {
		if let value = value as? String {
			return (value as NSString).floatValue
		}
		return nil
	}

	func transformToJSON(value: Object?) -> String? {
		if let value = value {
			return value.description
		}
		return nil
	}
}