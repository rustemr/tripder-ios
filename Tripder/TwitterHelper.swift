//
//  TwitterHelper.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import TwitterKit
import SwiftyUserDefaults
import ObjectMapper
import SwiftyJSON

class TwitterHelper: NSObject {
    class var sharedInstance: TwitterHelper {
        struct Singleton {
            static let instance = TwitterHelper()
        }
        return Singleton.instance
    }
    
    private override init() {
        super.init()
    }
    
    var user: TwitterUser!
    
    func refreshUserInfo(completionHandler: (Bool -> Void)?) {
        if !Defaults.hasKey(kTWTRUserIDKey) {
            return
        }
        let usersEndpoint = "https://api.twitter.com/1.1/users/show.json"
        let params = ["user_id": Defaults[kTWTRUserIDKey].string!]
        var error: NSError?
        
        let request = Twitter.sharedInstance().APIClient.URLRequestWithMethod("GET", URL: usersEndpoint, parameters: params, error: &error)
        if request != nil {
            Twitter.sharedInstance().APIClient.sendTwitterRequest(request, completion: { response, data, requestError in
                if requestError != nil {
                    println(requestError.localizedDescription)
                } else {
                    var json = JSON(data: data)
                    self.user = Mapper<TwitterUser>().map(json.rawValue)
                    self.user.profileImageURL = NSURL(string: self.user.profileImageURL.absoluteString!.stringByReplacingOccurrencesOfString("_normal", withString: "_bigger"))
                }
            })
        }
    }
    
    func loadUser(screenName: String, completion: (TwitterUser! -> Void)?) {
        let usersEndpoint = "https://api.twitter.com/1.1/users/show.json"
        let params = ["screen_name": screenName]
        var error: NSError?
        
        let request = Twitter.sharedInstance().APIClient.URLRequestWithMethod("GET", URL: usersEndpoint, parameters: params, error: &error)
        if request != nil {
            Twitter.sharedInstance().APIClient.sendTwitterRequest(request, completion: { response, data, requestError in
                if requestError != nil {
                    println(requestError.localizedDescription)
                    completion?(nil)
                } else {
                    var json = JSON(data: data)
                    var user = Mapper<TwitterUser>().map(json.rawValue)!
                    user.profileImageURL = NSURL(string: user.profileImageURL.absoluteString!
                        .stringByReplacingOccurrencesOfString("_normal", withString: "_bigger"))
                    completion?(user)
                }
            })
        }
    }
}

class TwitterUser: Mappable {
    var name: String!
    var bio: String!
    var backgroundImageURL: NSURL!
    var profileImageURL: NSURL!
    
    required init() { }
    
    //MARK: - Mappable
    func mapping(map: Map) {
        name                <- map["name"]
        bio                 <- map["description"]
        backgroundImageURL  <- (map["profile_banner_url"], URLTransform())
        profileImageURL     <- (map["profile_image_url"], URLTransform())
    }
}