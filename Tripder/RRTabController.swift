//
//  RRTabController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

class RRTabController: UIViewController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "RRTabController", bundle: NSBundle.mainBundle())
        self.loadView()
    }
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var indicator1: UIView!
    @IBOutlet weak var indicator2: UIView!
    @IBOutlet weak var indicator3: UIView!
    @IBOutlet weak var indicator4: UIView!
    
    var tabButtons = [UIButton]()
    var tabIndicators = [UIView]()
    
    var viewControllers: [UIViewController] = []
    
    var selectedViewController: UIViewController! {
        didSet {
            oldValue?.removeFromParentViewController()
            oldValue?.view.removeFromSuperview()
            
            selectedViewController.willMoveToParentViewController(self)
            self.addChildViewController(selectedViewController)
            selectedViewController.view.frame = contentView.bounds
            contentView.addSubview(selectedViewController.view)
            selectedViewController.didMoveToParentViewController(self)
            
            if let navigationController = selectedViewController as? UINavigationController {
                navigationController.popToRootViewControllerAnimated(true)
            }
        }
    }
    var selectedIndex: Int! {
        didSet {
            self.selectedViewController = viewControllers[selectedIndex]
            reloadTabButtons()
        }
    }
    
    override func loadView() {
        super.loadView()
        tabButtons = [button1, button2, button3, button4]
        tabIndicators = [indicator1, indicator2, indicator3, indicator4]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        if selectedViewController == nil {
            selectedIndex = 0
        }
    }
    
    func reloadTabButtons() {
        tabIndicators.map({ $0.hidden = true })
        tabIndicators[selectedIndex].hidden = false
    }
    
    @IBAction func clickTab(sender: UIButton) {
        selectedIndex = find(tabButtons, sender)
    }
}

extension RRTabController {
    class func tripDerController() -> RRTabController {
        var searchController = SearchViewController()
        var tabController = RRTabController()
        var nav2 = UINavigationController(rootViewController: searchController)
        nav2.setNavigationBarHidden(true, animated: false)
        var nav3 = UINavigationController(rootViewController: MatchViewController())
        nav3.setNavigationBarHidden(true, animated: false)
        tabController.viewControllers = [ProfileViewController(), nav2, nav3, UIViewController()]
        tabController.selectedIndex = 1
        return tabController
    }
}