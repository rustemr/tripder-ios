//
//  ProfileView.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

class ProfileView: UIView {

    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var imageBack: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBio: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    func configure() {
        var view = NSBundle.mainBundle().loadNibNamed("ProfileView", owner: self, options: nil)[0] as UIView
        self.addSubview(view)
        view.autoresizingMask = .FlexibleHeight
        
        imageUser.layer.masksToBounds = true
        imageUser.layer.cornerRadius = imageUser.bounds.width / 2
    }
    
    func reloadAppearance() {
        var user = TwitterHelper.sharedInstance.user
        labelName.text = user.name
        labelBio.text = user.bio
        imageUser.sd_setImageWithURL(user.profileImageURL)
        imageBack.sd_setImageWithURL(user.backgroundImageURL)
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSizeMake(320, 170)
    }
}
