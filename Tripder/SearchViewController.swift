//
//  SearchViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import ObjectMapper

let searchDateFormatter: NSDateFormatter = {
    var formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter
}()

let pickerDateFormatter: NSDateFormatter = {
    var formatter = NSDateFormatter()
    formatter.dateFormat = "dd LLL"
    return formatter
    }()

class SearchViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "SearchViewController", bundle: NSBundle.mainBundle())
    }
    
    @IBOutlet var optionButtons: [UIButton]!
    @IBOutlet weak var textFieldWhere: UITextField!
    @IBOutlet weak var textFieldDepart: UITextField!
    @IBOutlet weak var textFieldReturn: UITextField!
    
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation(latitude: 37.7699937, longitude: -122.4448403)
    
    var enabledButtons = [1, 2, 3]
    var dateDepart = NSDate(timeIntervalSinceNow: 24*3600)
    var dateReturn = NSDate(timeIntervalSinceNow: 7*24*3600)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Add pickers for dates
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadEnabledButtons()
        reloadDates()
        
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
    }
    
    func reloadEnabledButtons() {
        for button in optionButtons {
            if enabledButtons.filter({ $0 == button.tag }).count > 0 {
                button.setBackgroundImage(UIImage(named: "select-button-on"), forState: .Normal)
                button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            } else {
                button.setBackgroundImage(UIImage(named: "select-button-off"), forState: .Normal)
                button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            }
        }
    }
    
    func reloadDates() {
        textFieldDepart.text = pickerDateFormatter.stringFromDate(dateDepart)
        textFieldReturn.text = pickerDateFormatter.stringFromDate(dateReturn)
    }
    
    //MARK: - Actions
    @IBAction func clickOption(sender: UIButton!) {
        if let index = find(enabledButtons, sender.tag) {
            enabledButtons.removeAtIndex(index)
        } else {
            enabledButtons.append(sender.tag)
        }
        reloadEnabledButtons()
    }
    
    @IBAction func clickGo(sender: AnyObject) {
        if currentLocation == nil {
            return
        }
        
        textFieldWhere.resignFirstResponder()
        
        var json = JSON([
            "date_from": searchDateFormatter.stringFromDate(dateDepart),
            "date_to": searchDateFormatter.stringFromDate(dateReturn),
            "options": join(",", enabledButtons.map({ $0.description })),
            "from": "\(currentLocation!.coordinate.latitude),\(currentLocation!.coordinate.longitude)",
            "to": textFieldWhere.text
            ])
        var hud = JGProgressHUD(style: .Dark)
        hud.textLabel.text = "Searching.."
        hud.showInView(self.view)
        Alamofire.request(Method.POST, BASE_URL + "search_expedia_4.php", parameters: ["params": json.rawString()!])
            .responseJSON ({ request, response, data, error in
                hud.dismiss()
                if error != nil {
                    println(error!.description)
                } else {
                    var json = JSON(data!)
                    
                    var hotels = [Hotel]()
                    if let array = json["hotels"].array {
                        for obj in array {
                            var hotel = Mapper<Hotel>().map(obj.rawValue)
                            hotel!.json = obj.rawValue
                            hotels.append(hotel!)
                        }
                    }
                    var cars = [Car]()
                    if let array = json["cars"].array {
                        for obj in array {
                            var car = Mapper<Car>().map(obj.rawValue)
                            car!.json = obj.rawValue
                            cars.append(car!)
                        }
                    }
                    var flightPack = Mapper<FlightPack>().map(json["flights"].rawValue)

                    if flightPack?.flight1?.carrier == nil {
                        flightPack = nil
                    } else {
                        flightPack!.json = json["flights"].rawValue
                    }

                    var resultsController = ResultsViewController()
                    resultsController.hotels = filter(hotels, { $0.price != nil})
                    resultsController.cars = filter(cars, { $0.price != nil})
                    resultsController.flightPack = flightPack
                    self.navigationController?.pushViewController(resultsController, animated: true)
                }
            })
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField == textFieldWhere {
            return true
        } else {
            
            return false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        locationManager.stopUpdatingLocation()
        currentLocation = locations.last as CLLocation
    }
}
