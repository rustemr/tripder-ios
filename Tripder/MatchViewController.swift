//
//  MatchViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class MatchViewController: UIViewController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "MatchViewController", bundle: NSBundle.mainBundle())
    }
    
    @IBOutlet weak var swipeableView: ZLSwipeableView!
    var trips: [Trip] = []
    var tripIndex = 0
    var controllers: [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        swipeableView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        var json = JSON([
            "date": searchDateFormatter.stringFromDate(NSDate()),
            "options": "1,2,3", //join(",", enabledButtons.map({ $0.description })),
            "from": "SFO", //"\(currentLocation!.coordinate.latitude),\(currentLocation!.coordinate.longitude)",
            "to": "BOS", //textFieldWhere.text
            "trip_id": "0"
            ])
        var hud = JGProgressHUD(style: .Dark)
        hud.textLabel.text = "Matching.."
        hud.showInView(self.view)
        Alamofire.request(Method.POST, BASE_URL + "search_mate.php", parameters: ["params": json.rawString()!])
            .responseJSON ({ request, response, data, error in
                if error != nil {
                    hud.dismiss()
                    println(error!.description)
                } else {
                    println(data!)
                    var json = JSON(data!)
                    self.trips = Mapper<Trip>().mapArray(json["trips"].rawValue)!
                    delay(2) {
                        self.tripIndex = 0
                        self.swipeableView.dataSource = self
                        self.swipeableView.loadNextSwipeableViewsIfNeeded()
                        hud.dismiss()
                    }
                }
            })
    }
}

extension MatchViewController: ZLSwipeableViewDelegate {
    func swipeableView(swipeableView: ZLSwipeableView!, didSwipeView view: UIView!, inDirection direction: ZLSwipeableViewDirection) {
        if direction == .Right {
            if let resultsController = controllers[view.tag] as? ResultsViewController {
                var paymentController = PaymentViewController()
                paymentController.flightPack = resultsController.flightPack
                paymentController.hotel = resultsController.currentHotel
                paymentController.car = resultsController.currentCar
                self.navigationController!.pushViewController(paymentController, animated: true)
            }
        }
    }
}

extension MatchViewController: ZLSwipeableViewDataSource {
    func nextViewForSwipeableView(swipeableView: ZLSwipeableView!) -> UIView! {
        if self.tripIndex < self.trips.count {
            var cardView = CardView(frame: self.swipeableView.bounds)
            var resultsController = ResultsViewController()
            resultsController.showsOnlyCurrent = true
            var trip = self.trips[tripIndex]
            resultsController.hotels = [trip.hotel]
            resultsController.cars = [trip.car]
            resultsController.flightPack = trip.flightPack
            resultsController.user = trip.user ?? TwitterHelper.sharedInstance.user
            resultsController.view.frame = cardView.bounds
            cardView.addSubview(resultsController.view)
            controllers.append(resultsController)
            cardView.tag = tripIndex
            
            self.tripIndex++
            return cardView
        }
        return nil
    }
}

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}
