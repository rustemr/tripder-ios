//
//  ViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import TwitterKit
import SwiftyUserDefaults

let kDGTSessionTokenKey = "kDGTSessionTokenKey"
let kDGTSessionTokenSecretKey = "kDGTSessionTokenSecretKey"
let kDGTPhoneNumberKey = "kDGTPhoneNumberKey"
let kDGTUserIDKey = "kDGTUserIDKey"

let kTWTRSessionTokenKey = "kTWTRSessionTokenKey"
let kTWTRSessionTokenSecretKey = "kTWTRSessionTokenSecretKey"
let kTWTRUserNameKey = "kTWTRUserNameKey"
let kTWTRUserIDKey = "kTWTRUserIDKey"

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var button = TWTRLogInButton { session, error in
            if session != nil {
                Defaults[kTWTRSessionTokenKey] = session.authToken
                Defaults[kTWTRSessionTokenSecretKey] = session.authTokenSecret
                Defaults[kTWTRUserNameKey] = session.userName
                Defaults[kTWTRUserIDKey] = session.userID
                TwitterHelper.sharedInstance.refreshUserInfo(nil)
                self.authorizeDigits()
            } else {
                println("error: \(error.localizedDescription)");
            }
        }
        button.center = self.view.center
        self.view.addSubview(button)
    }
    
    func authorizeDigits() {
        Digits.sharedInstance().authenticateWithCompletion { (session, error) in
            if error == nil {
                Defaults[kDGTSessionTokenKey] = session.authToken
                Defaults[kDGTSessionTokenSecretKey] = session.authTokenSecret
                Defaults[kDGTPhoneNumberKey] = session.phoneNumber
                Defaults[kDGTUserIDKey] = session.userID
                
                self.navigationController!.setViewControllers([RRTabController.tripDerController()], animated: true)
            }
        }
    }

}

