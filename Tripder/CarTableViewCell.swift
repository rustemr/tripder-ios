//
//  CarTableViewCell.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewPicture: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var labelPassengers: UILabel!
    @IBOutlet weak var labelLuggage: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    var car: Car! {
        didSet {
            imageViewPicture.sd_setImageWithURL(car.pictureURL)
            labelName.text = car.model
            labelClass.text = car.carClass
            labelPassengers.text = "Seats: \(car.passengers)"
            labelLuggage.text = "Luggage: \(car.luggage)"
            labelPrice.text = String(format: "$%.0f", car.price)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
