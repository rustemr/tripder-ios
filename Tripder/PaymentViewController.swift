//
//  PaymentViewController.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 3/1/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import ObjectMapper
import TwitterKit

class PaymentViewController: UIViewController {

    @IBOutlet weak var textFieldCard: UITextField!
    @IBOutlet weak var textFieldExpires: UITextField!
    @IBOutlet weak var textFieldCVV: UITextField!
    
    var car: Car!
    var hotel: Hotel!
    var flightPack: FlightPack!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience override init() {
        self.init(nibName: "PaymentViewController", bundle: NSBundle.mainBundle())
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickNext(sender: AnyObject) {
        var params = [String: AnyObject]()
        if self.car != nil {
            params["car"] = self.car.json
        }
        if self.hotel != nil {
            params["hotel"] = self.hotel.json
        }
        if self.flightPack != nil {
            params["flight"] = self.flightPack.json
        }
        var json = JSON(params)
        var hud = JGProgressHUD(style: .Dark)
        hud.textLabel.text = "Requesting.."
        hud.showInView(self.view)
        Alamofire.request(Method.POST, BASE_URL + "create_trip.php", parameters: ["params": json.rawString()!])
            .responseJSON ({ request, response, data, error in
                hud.dismiss()
                if error != nil {
                    println(error!.description)
                } else {
//                    println(data!)
                    var json = JSON(data!)
                    var tripID = json["trip_id"].int!.description
                    var link = BASE_URL + "share/" + tripID
                    
                    let composer = TWTRComposer()
                    composer.setText("Share my trip! It's gonna be awesome! " + link)
                    composer.showWithCompletion { result in
                        if result == TWTRComposerResult.Cancelled {
                            println("Tweet composition cancelled")
                        } else {
                            println("Sending tweet!")
                        }
                        self.navigationController!.popToRootViewControllerAnimated(true)
                    }
                }
            })

    }
}

extension PaymentViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldCard {
            if (range.location == 19) {
                textFieldExpires.becomeFirstResponder()
                return false
            }
            
            // Backspace
            if countElements(string) == 0 {
                return true
            }
            
            if (range.location == 4) || (range.location == 9) || (range.location == 14) {
                textField.text   = textField.text + " "
            }
            
            return true
        } else if textField == textFieldCVV {
            if (range.location == 3) {
                textField.resignFirstResponder()
                return false
            }
        } else if textField == textFieldExpires {
            if (range.location == 5) {
                textFieldCVV.becomeFirstResponder()
                return false
            }
            
            // Backspace
            if countElements(string) == 0 {
                return true
            }
            
            if range.location == 2 {
                textField.text = textField.text + "/"
            }
            
            return true
        }
        
        return true
    }
}