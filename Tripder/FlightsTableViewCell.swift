//
//  FlightsTableViewCell.swift
//  Tripder
//
//  Created by Rustem Ramazanov on 2/28/15.
//  Copyright (c) 2015 Rustem Ramazanov. All rights reserved.
//

import UIKit

let flightTimeFormatter: NSDateFormatter = {
    var formatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    formatter.dateStyle = .NoStyle
    formatter.timeStyle = .ShortStyle
    return formatter
}()

class FlightsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewPicture: UIImageView!
    @IBOutlet weak var labelFlight1Route: UILabel!
    @IBOutlet weak var labelFlight1Info: UILabel!
    @IBOutlet weak var labelFlight2Route: UILabel!
    @IBOutlet weak var labelFlight2Info: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    var flightPack: FlightPack! {
        didSet {
            labelFlight1Route.text = "\(flightPack.flight1.departureCode) -> \(flightPack.flight1.arrivalCode)"
            labelFlight1Info.text = "\(flightPack.flight1.carrier) \(flightPack.flight1.number) \(flightTimeFormatter.stringFromDate(flightPack.flight1.departureDatetime))"
            labelFlight2Route.text = "\(flightPack.flight2.departureCode) -> \(flightPack.flight2.arrivalCode)"
            labelFlight2Info.text = "\(flightPack.flight2.carrier) \(flightPack.flight2.number) \(flightTimeFormatter.stringFromDate(flightPack.flight2.departureDatetime))"
//            imageViewPicture.sd_setImageWithURL(flightPack.pictureURL)
            imageViewPicture.image = UIImage(named: "united_logo")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
